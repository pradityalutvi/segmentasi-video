package otsu;

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Otsu {
    public static void main (String args[]) throws IOException{
//        System.out.println("Otsu Thresholder Demo - A.Greensted - http://www.labbookpages.co.uk");
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	new Otsu("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\digunakan\\diff1_1020.png", 1);
	new Otsu("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\digunakan\\diff2_1020.png", 1);
    }
    
    public Otsu(String filename, int i) throws IOException{
        // Load Source 
        Mat src = Imgcodecs.imread(filename,0);
        System.out.println(src.size());
        MatOfByte mem = new MatOfByte();
        Imgcodecs.imencode(".bmp", src, mem);
	BufferedImage srcImage = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
        byte[] srcData = ((DataBufferByte) srcImage.getRaster().getDataBuffer()).getData(0);
	
	int width = srcImage.getWidth();
	int height = srcImage.getHeight();

        // Sanity check image
        if (width * height  != srcData.length) {
                System.err.println("Unexpected image data size. Should be greyscale image");
                System.exit(1);
        }

//        // Output Image info
        System.out.println(width*height);
        System.out.printf("Loaded image: %s, width: %d, height: %d, num bytes: %d\n", filename, width, height, srcData.length);
        System.out.println(srcData);

        byte[] dstData = new byte[srcData.length];
        // Create Otsu Thresholder
        OtsuThresholder thresholder = new OtsuThresholder();
        int threshold = thresholder.doThreshold(srcData, dstData);

        System.out.printf("Threshold: %d\n", threshold);
        Imgproc.threshold(src, src, threshold, 255, Imgproc.THRESH_BINARY);
        Imgcodecs.imwrite("E:\\binary.png", src);
        // Create GUI
        GreyFrame srcFrame = new GreyFrame(width, height, srcData);
        GreyFrame dstFrame = new GreyFrame(width, height, dstData);
        GreyFrame histFrame = createHistogramFrame(thresholder);

        JPanel infoPanel = new JPanel();
        infoPanel.add(histFrame);

        JPanel panel = new JPanel(new BorderLayout(5, 5));
        panel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
        panel.add(infoPanel, BorderLayout.NORTH);
        panel.add(srcFrame, BorderLayout.WEST);
        panel.add(dstFrame, BorderLayout.EAST);
        panel.add(new JLabel("A.Greensted - http://www.labbookpages.co.uk - Modify By Prad.", JLabel.CENTER), BorderLayout.SOUTH);

        JFrame frame = new JFrame("Blob Detection Demo - "+filename);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(panel);
        frame.pack();
        frame.setVisible(true);

        // Save Images
        try
        {
                int dotPos = filename.lastIndexOf(".");
                String basename = filename.substring(0,dotPos);

                javax.imageio.ImageIO.write(dstFrame.getBufferImage(), "PNG", new File(basename+"_BW.png"));
                javax.imageio.ImageIO.write(histFrame.getBufferImage(), "PNG", new File(basename+"_hist.png"));
        }
        catch (IOException ioE)
        {
                System.err.println("Could not write image " + filename);
        }
    }

    private GreyFrame createHistogramFrame(OtsuThresholder thresholder){
        int numPixels = 256 * 100;
        byte[] histPlotData = new byte[numPixels];

        int[] histData = thresholder.getHistData();
        int max = thresholder.getMaxLevelValue();
        int threshold = thresholder.getThreshold();

        for (int l=0 ; l<256 ; l++)
        {
                int ptr = (numPixels - 256) + l;
                int val = (100 * histData[l]) / max;

                if (l == threshold)
                {
                        for (int i=0 ; i<100 ; i++, ptr-=256) histPlotData[ptr] = (byte) 128;
                }
                else
                {
                        for (int i=0 ; i<100 ; i++, ptr-=256) histPlotData[ptr] = (val < i) ? (byte) 255 : 0;
                }
        }

        return new GreyFrame(256, 100, histPlotData);
    }
}