package tugasakhir;

import com.gluonhq.charm.glisten.control.ProgressBar;
import java.awt.Desktop;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
//import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.opencv.core.Core;
import static org.opencv.core.CvType.CV_8UC1;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.contourArea;
import static org.opencv.imgproc.Imgproc.resize;
import static org.opencv.imgproc.Imgproc.threshold;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 * FXML Controller class
 *
 * @author A S U S
 */
public class AppController implements Initializable {
    @FXML
    private Label txtNowPlaying;
    @FXML
    private Label txtMinTime;
    @FXML
    private Label txtMaxTime;
    @FXML
    private ImageView btnAddMedia;
    @FXML
    private Label btnAddMedia2;
    @FXML
    private ImageView videoPanel;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TableView tabelHasil;
    @FXML
    private TableColumn<ClipVideo, String> awalCol;
    @FXML
    private TableColumn<ClipVideo, String> akhirCol;
    @FXML
    private TableColumn<ClipVideo, String> idCol;
    @FXML
    private ImageView videoProsesPanel;
    @FXML
    private CheckBox cbShowProses;
    @FXML
    private Button btnNewROI;
    @FXML
    private Button btnProses;
    @FXML
    private ImageView imgLoading;
 
    
    int fps, postFrame, postFrame1, postFrame2, time, second = 0, resolusi = 360, ambangBatas = 30, id = 1, konstantaLoncat = 10, minArea = 100;
    int x, x2, y, y2;
    VideoCapture video = null;
    Mat frame1;
    Mat frame2;
    Mat frame3;
    Mat deltaFrame1;
    Mat deltaFrame2;
    Mat hasilProses;
    Mat show;
    double ratioframe = 1.763157;
    private String nameFile, path = null, typeFile;
    MatOfByte mem = null;
    MatOfByte memHasil = null;
    DaemonThread myThread = null;
    Thread t = null;
    List<MatOfPoint> cnts;
    ClipVideo clip = new ClipVideo();
    boolean roiAda = false;
    Rect roi;
    @FXML
    private ImageView btnSetting;
    @FXML
    private TabPane settingPane;
    @FXML
    private CheckBox cbSetTresh;
    @FXML
    private Slider sliderTresh;
    @FXML
    private Label lblTresh;
    @FXML
    private CheckBox cbKonstanta;
    @FXML
    private Slider sliderKonstanta;
    @FXML
    private Label lblKonstanta;
    @FXML
    private CheckBox cbSetMinArea;
    @FXML
    private Slider sliderMinArea;
    @FXML
    private Label lblMinArea;
    @FXML
    private Button btnStop;
    
    
    @FXML
    private void btnCloseOnClick(){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Ingin keluar dari program?");
        Optional<ButtonType> act = alert.showAndWait();
        if(act.get() == ButtonType.OK){
            File f = new File("E:\\KULIAH\\Tugas Akhir\\Data Video\\dummy\\");
            File[] fs = f.listFiles();
            for(File i:fs){
                i.delete();
            }
            System.exit(0);
        }
    }
    
    @FXML
    private void btnMinOnClick(ActionEvent evt){
        Stage s = (Stage)((Node)evt.getSource()).getScene().getWindow();
        s.setIconified(true);
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        setColumProperties();
        setMouseListenerOnVideoPanel();
        setSliderSettingListener();
        videoPanel.setVisible(false);
        btnStop.setVisible(false);
//        videoProsesPanel.setVisible(false);
        imgLoading.setVisible(false);
        settingPane.setVisible(false);
        sliderTresh.setDisable(true);
        sliderTresh.setValue(ambangBatas);
        sliderKonstanta.setDisable(true);
        sliderKonstanta.setValue(konstantaLoncat);
        sliderMinArea.setDisable(true);
        sliderMinArea.setValue(minArea);
    }

    private void setSliderSettingListener(){
        sliderTresh.valueProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                sliderTresh.setValue(newValue.intValue());
                ambangBatas = (int) sliderTresh.getValue();
                lblTresh.setText(""+ (int) sliderTresh.getValue());
            }
            
        });
        sliderKonstanta.valueProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                sliderKonstanta.setValue(newValue.intValue());
                konstantaLoncat = (int) sliderKonstanta.getValue();
                lblKonstanta.setText(""+ (int) sliderKonstanta.getValue());
            }
            
        });
        sliderMinArea.valueProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                sliderMinArea.setValue(newValue.intValue());
                minArea = (int) sliderMinArea.getValue();
                lblMinArea.setText(""+ (int) sliderMinArea.getValue());
            }
            
        });
    }
    private void setStartPointROI(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    private void setEndPointROI(int x, int y){
        this.x2 = x;
        this.y2 = y;
    }
    
    private void setMouseListenerOnVideoPanel() {
        x = x2 = y = y2 = 0;
        videoPanel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if(!roiAda){
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setHeaderText(null);
                    alert.setContentText("Klik, drag, kemudian lepaskan mouse di area frame video untuk membuat ROI (Region Of Interest)"
                            + "\nKlik tombol Set New ROI jika ingin merubah area ROI");
                    alert.showAndWait();
                }
            }
            
        });
        videoPanel.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if(!roiAda){
                    setStartPointROI((int) event.getX(), (int) event.getY());
                    if((int)event.getX() > show.width()){
//                        System.out.println("x kelebihan");
                        x = show.width() - 3;
                    } else if((int)event.getX() <= 0){
//                        System.out.println("x kekurangan");
                        x = 3;
                    }
                    if((int)event.getY() > show.height()){
//                        System.out.println("y kelebihan");
                        y = show.height() - 3;
                    } else if((int)event.getY() <= 0){
//                        System.out.println("y kekurangan");
                        y = 3;
                    }
                }
            } 
        });
        videoPanel.setOnMouseDragged(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if(!roiAda){
                    setEndPointROI((int) event.getX(), (int) event.getY());
                    if((int)event.getX() > show.width()){
//                        System.out.println("x kelebihan");
                        x2 = show.width() - 3;
                    } else if((int)event.getX() <= 0){
//                        System.out.println("x kekurangan");
                        x2 = 3;
                    }
                    if((int)event.getY() > show.height()){
//                        System.out.println("y kelebihan");
                        y2 = show.height() - 3;
                    } else if((int)event.getY() <= 0){
//                        System.out.println("y kekurangan");
                        y2 = 3;
                    }
//                    System.out.println("Drag on "+event.getX()+", "+event.getY());
                    roi = new Rect(new Point(x,y), new Point(x2,y2));
                    Mat dummy = new Mat();
                    show.copyTo(dummy);
                    Imgproc.rectangle(dummy, roi, new Scalar(0, 255, 255), 2, 0);
                    resize(dummy, dummy, new Size(resolusi*ratioframe, resolusi));
                    Imgcodecs.imencode(".jpg", dummy, mem);
                    java.awt.Image im = null;
                    try {
                        im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
                        BufferedImage buff = (BufferedImage) im;
                        Image image = SwingFXUtils.toFXImage(buff, null);
                        videoPanel.setImage(image);
                    } catch (IOException ex) {
                        System.out.println("exception mouse dragged");
                    }
                }
            }
        });
        videoPanel.setOnMouseReleased(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if(!roiAda){
                    setEndPointROI((int) event.getX(), (int) event.getY());
                    if((int)event.getX() > show.width()){
//                        System.out.println("x kelebihan");
                        x2 = show.width() - 3;
                    } else if((int)event.getX() <= 0){
//                        System.out.println("x kekurangan");
                        x2 = 3;
                    }
                    if((int)event.getY() > show.height()){
//                        System.out.println("y kelebihan");
                        y2 = show.height() - 3;
                    } else if((int)event.getY() <= 0){
//                        System.out.println("y kekurangan");
                        y2 = 3;
                    }
//                    System.out.println("Release "+event.getX()+", "+event.getY());
//                    System.out.println("point1 ("+x+","+y+"). dan point2 ("+x2+","+y2+")");
                    roi = new Rect(new Point(x,y), new Point(x2,y2));
                    Mat dummy = new Mat();
                    show.copyTo(dummy);
                    Imgproc.rectangle(dummy, roi, new Scalar(0, 0, 255), 2, 0);
                    roiAda = true;
                    resize(dummy, dummy, new Size(resolusi*ratioframe, resolusi));
                    Imgcodecs.imencode(".jpg", dummy, mem);
                    java.awt.Image im = null;
                    try {
                        im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
                        BufferedImage buff = (BufferedImage) im;
                        Image image = SwingFXUtils.toFXImage(buff, null);
                        videoPanel.setImage(image);
                    } catch (IOException ex) {
                        System.out.println("exception mouse released");
                    }
                }
            }
        });
    }
    
    private <S,T> TableColumn<S,T> column(String title, Function<S, ObservableValue<T>> property) {
        TableColumn<S,T> col = new TableColumn<>(title);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        return col ;
    }
    
    private void setColumProperties(){
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        awalCol.setCellValueFactory(new PropertyValueFactory<>("detikAwal"));
        akhirCol.setCellValueFactory(new PropertyValueFactory<>("detikAkhir"));
        TableColumn<ClipVideo, ClipVideo> actionCol = column("Action", ReadOnlyObjectWrapper<ClipVideo>::new);
        actionCol.setSortable(false);
        actionCol.setPrefWidth(110);
        tabelHasil.getColumns().add(actionCol);
        actionCol.setCellFactory(col -> {
            TableCell<ClipVideo, ClipVideo> cell = new TableCell<ClipVideo, ClipVideo>(){
                Button trimBtn = new Button();
                Button playBtn = new Button();
                Image imgTrim = new Image("/image/trim_exited.png");
                Image imgPlay = new Image("/image/play.png");
                ImageView iv = new ImageView();
                ImageView iv2 = new ImageView();
                @Override
                public void updateItem(ClipVideo clip, boolean empty) {
                    super.updateItem(clip, empty);
                    if (empty) {
                        setGraphic(null);
                    } else {
                        // tombol play clip
                        playBtn.setOnAction(e ->{
                            // memutar video berdasarkan clip yang dipilih
                            ClipVideo save = getTableView().getItems().get(getIndex());
                            String dummyPath = "E:\\KULIAH\\Tugas Akhir\\Data Video\\dummy\\clipID_"+save.getId()+typeFile;
//                            System.out.println(dummyPath);
                            try {
                                Desktop.getDesktop().open(new File(dummyPath));
                            } catch (IOException ex) {
                                System.out.println("exception play clip");
                            }
                        });
                        playBtn.setOnMouseEntered(e ->{
                            imgPlay = new Image("/image/play_entered.png");
                            playBtn.setCursor(Cursor.HAND);
                            iv2.setImage(imgPlay);
                        });
                        playBtn.setOnMouseExited(e ->{
                            imgPlay = new Image("/image/play.png");
                            iv2.setImage(imgPlay);
                        });
                        // end tombol play clip, start tombol trim
                        trimBtn.setOnAction(e ->{
                            JFileChooser fc = new JFileChooser("E:\\KULIAH\\Tugas Akhir\\Data Video\\save\\");
                            fc.setDialogTitle("Select Directories to Save New Clip Video");
                            int returnValue = fc.showSaveDialog(new JFrame());
                            if(returnValue == JFileChooser.APPROVE_OPTION){
                                ClipVideo save = getTableView().getItems().get(getIndex());
                                String dscPath = fc.getSelectedFile().getAbsolutePath();
                                Alert alert = new Alert(AlertType.INFORMATION);;
                                alert.setHeaderText(null);
                                alert.setTitle("Information Dialog");
                                dscPath = dscPath+typeFile;
//                                System.out.println(dscPath);
//                                System.exit(0);
                                try {
                                    save.trimVideo(path, dscPath);
                                    alert.setContentText("Penyimpanan Clip Video Berhasil");
                                } catch (IOException ex) {
                                    alert.setContentText("Penyimpanan Clip Video Gagal");
                                } finally{
                                    alert.showAndWait();
                                }
                            }
                        });
                        trimBtn.setOnMouseEntered(e ->{
                            imgTrim = new Image("/image/trim_entered.png");
                            trimBtn.setCursor(Cursor.HAND);
                            iv.setImage(imgTrim);
                        });
                        trimBtn.setOnMouseExited(e ->{
                            imgTrim = new Image("/image/trim_exited.png");
                            iv.setImage(imgTrim);
                        });
                        //end tombol trim
                        trimBtn.setStyle("-fx-background-color: transparent;");
                        playBtn.setStyle("-fx-background-color: transparent;");
                        iv.setImage(imgTrim);
                        iv2.setImage(imgPlay);
                        iv.setPreserveRatio(true);
                        iv2.setPreserveRatio(true);
                        playBtn.setGraphic(iv2);
                        trimBtn.setGraphic(iv);
                        HBox pane = new HBox (playBtn, trimBtn);
                        setGraphic(pane);
                    }
                }
            };
            return cell;
        });
    }

    @FXML
    private void addVideo(MouseEvent event) {
        JFileChooser fc = new JFileChooser("E:\\KULIAH\\Tugas Akhir\\Data Video\\");
        fc.setDialogTitle("Choose a CCTV Video..");
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Video Files", "mp4", "avi");
        fc.setFileFilter(filter);
        fc.setMultiSelectionEnabled(false);
        fc.setAcceptAllFileFilterUsed(false);
        int returnValue = fc.showOpenDialog(new JFrame());
        if(returnValue == JFileChooser.APPROVE_OPTION){
            tabelHasil.getItems().clear();
            videoPanel.setVisible(true);
            btnSetting.setDisable(false);
            path = fc.getSelectedFile().getAbsolutePath();
//            System.out.println(path);
            nameFile = fc.getSelectedFile().getName();
            typeFile = nameFile.substring(nameFile.lastIndexOf("."), nameFile.length());
            txtNowPlaying.setText("Now Playing : "+nameFile);
            btnAddMedia.setVisible(false);
            btnAddMedia2.setVisible(false);
            show = new Mat();
            video = new VideoCapture(path);
            fps = (int) video.get(Videoio.CAP_PROP_FPS);
            time = (int) (video.get(Videoio.CAP_PROP_FRAME_COUNT) / fps);
            System.out.println("fps:"+video.get(Videoio.CAP_PROP_FPS) +", totalframe:"+video.get(Videoio.CAP_PROP_FRAME_COUNT));
            System.out.println("waktu :"+(video.get(Videoio.CAP_PROP_FRAME_COUNT) / video.get(Videoio.CAP_PROP_FPS)));
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, time);
            txtMaxTime.setText(new SimpleDateFormat("HH:mm:ss").format(calendar.getTime()));
            video.read(show);
            resize(show, show, new Size(resolusi*ratioframe, resolusi));
            mem = new MatOfByte();
            Imgcodecs.imencode(".jpg", show, mem);
            try {
                java.awt.Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
                BufferedImage buff = (BufferedImage) im;
                Image image = SwingFXUtils.toFXImage(buff, null);
                videoPanel.setImage(image);
                setWindow(videoPanel);
            } catch (IOException ex) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Input video gagal.");
                alert.showAndWait();
            }
        }
    }
    
    private void setWindow(ImageView iv) {
        Image img = iv.getImage();
        double w, h;
        double ratioX = iv.getFitWidth() / img.getWidth();
        double ratioY = iv.getFitHeight() / img.getHeight();
        double reducCoeff;
        if (ratioX >= ratioY) {
            reducCoeff = ratioY;
        } else {
            reducCoeff = ratioX;
        }
        w = img.getWidth() * reducCoeff;
        h = img.getHeight() * reducCoeff;
        iv.setX((iv.getFitWidth() - w) / 2);
        iv.setY((iv.getFitHeight() - h) / 2);
    }

    @FXML
    private void btnProsesOnClicked (MouseEvent event) throws IOException {
        if(video != null){
            if(!roiAda || (roi.area() <= 5000)){
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning Dialog");
                alert.setHeaderText(null);
                alert.setContentText("ROI(Region Of Interest) belum diatur atau area terlalu kecil. \nSilahkan atur ulang");
                alert.showAndWait();
                roiAda = false;
                mem = new MatOfByte();
                Imgcodecs.imencode(".jpg", show, mem);
                java.awt.Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
                BufferedImage buff = (BufferedImage) im;
                Image image = SwingFXUtils.toFXImage(buff, null);
                videoPanel.setImage(image);
                setWindow(videoPanel);
            } else{
                btnStop.setVisible(true);
                btnNewROI.setDisable(true);
                btnProses.setDisable(true);
                btnProses.setText(null);
                imgLoading.setVisible(true);
                settingPane.setVisible(false);
                Image btnSet = new Image("/image/setting_disabled.png");
                btnSetting.setImage(btnSet);
                btnSetting.setDisable(true);
                myThread = new DaemonThread();
                t = new Thread(myThread);
                t.setDaemon(true);
                myThread.runnable = true;
                t.start();
            }
        } else{
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Belum ada data video yang diload.");
            alert.showAndWait();            
        }
    }

    @FXML
    private void showProsesClicked(MouseEvent event) {
        if(cbShowProses.isSelected()){
            videoProsesPanel.setVisible(true);
        } else{
            videoProsesPanel.setVisible(false);
            
        }
    }

    @FXML
    private void setNewRoiOnAction(ActionEvent event) throws IOException {
        if(video != null){
            
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("ROI(Region Of Interest) dapat diatur kembali.");
            alert.showAndWait();
            roiAda = false;
            mem = new MatOfByte();
            Imgcodecs.imencode(".jpg", show, mem);
            java.awt.Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
            BufferedImage buff = (BufferedImage) im;
            Image image = SwingFXUtils.toFXImage(buff, null);
            videoPanel.setImage(image);
            setWindow(videoPanel);
        }
    }

    @FXML
    private void btnSettingOnExited(MouseEvent event) {
        Image btnSet = new Image("/image/setting_exited.png");
        btnSetting.setImage(btnSet);
    }

    @FXML
    private void btnSettingOnEntered(MouseEvent event) {
        Image btnSet = new Image("/image/setting_entered.png");
        btnSetting.setImage(btnSet);
    }

    @FXML
    private void btnSettingOnClicked(MouseEvent event) {
        if(settingPane.isVisible()){
            settingPane.setVisible(false);
        } else{
            settingPane.setVisible(true);
        }
    }

    @FXML
    private void setTresh2def(MouseEvent event) {
        if(cbSetTresh.isSelected()){
            ambangBatas = 30;
            sliderTresh.setValue(ambangBatas);
            sliderTresh.setDisable(true);
            lblTresh.setText(""+ambangBatas);
        } else{
            sliderTresh.setDisable(false);
        }
    }

    @FXML
    private void setKonstanta2def(MouseEvent event) {
        if(cbKonstanta.isSelected()){
            konstantaLoncat = 10;
            sliderKonstanta.setDisable(true);
            sliderKonstanta.setValue(konstantaLoncat);
            lblKonstanta.setText(""+konstantaLoncat);
        } else{
            sliderKonstanta.setDisable(false);
        }
    }

    @FXML
    private void setMinArea2def(MouseEvent event) {
        if(cbSetMinArea.isSelected()){
            minArea = 100;
            sliderMinArea.setValue(minArea);
            sliderMinArea.setDisable(true);
            lblMinArea.setText(""+minArea);
        } else{
            sliderMinArea.setDisable(false);
        }
    }

    @FXML
    private void stop(MouseEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Anda yakin menghentikan proses segmentasi?");
        Optional<ButtonType> act = alert.showAndWait();
        if(act.get() == ButtonType.OK){
            initialUlang();
            myThread.runnable = false;
        }
    }
    
    private class DaemonThread implements Runnable{

        protected volatile boolean runnable = false;
        Calendar timeVideo = Calendar.getInstance();
        
        public DaemonThread() {
            timeVideo.set(Calendar.HOUR_OF_DAY, 0);
            timeVideo.set(Calendar.MINUTE, 0);
            timeVideo.set(Calendar.SECOND, second);
        }
        
        @Override
        public void run() {
            synchronized (this) {
                frame3 = new Mat();
                frame2 = new Mat();
                frame1 = new Mat();
                deltaFrame1 = new Mat();
                deltaFrame2 = new Mat();
                memHasil = new MatOfByte();
                video = new VideoCapture(path);
                video.read(show);
                resize(show, show, new Size(resolusi*ratioframe, resolusi));
                Imgproc.rectangle(show, roi, new Scalar(0, 0, 255), 2, 0);
                frame1 = show.submat(roi);
                Imgproc.cvtColor(frame1, frame1, Imgproc.COLOR_BGR2GRAY);
                Imgproc.GaussianBlur(frame1, frame1, new Size(5, 5), 0);
                frame1.copyTo(frame2);
                frame1.copyTo(frame3);
                postFrame1 = postFrame2 = postFrame = 0;
                while(runnable){
                    if(video.grab()){
                        if (postFrame % (konstantaLoncat) == 0) {
                            try{
                                video.retrieve(show);
//                                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\pre\\sampel_" + postFrame2 + ".png", show);
                                resize(show, show, new Size(resolusi*ratioframe, resolusi));
//                                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\pre\\resize_" + postFrame2 + ".png", show);
                                Imgproc.rectangle(show, roi, new Scalar(0, 0, 255), 2, 0);
                                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\pre\\setROI_" + postFrame2 + ".png", show);
                                frame3 = show.submat(roi);
//                                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\pre\\frameROI_" + postFrame2 + ".png", frame3);

                                // prepocessing inti
                                Imgproc.cvtColor(frame3, frame3, Imgproc.COLOR_BGR2GRAY);
//                                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\pre\\gray_" + postFrame2 + ".png", frame3);
                                Imgproc.GaussianBlur(frame3, frame3, new Size(5, 5), 0);
//                                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\pre\\gaussian_" + postFrame2 + ".png", frame3);
                                
                                threeFrameDiff(frame1, frame2, frame3); // proses 3 frame difference
                                frame2.copyTo(frame1);// untuk proses iterasi berikutnya
                                frame3.copyTo(frame2);

                                postFrame1 = postFrame2;
                                postFrame2 = postFrame;
                                
                                //prepare nampilin video asli
                                resize(show, show, new Size(resolusi*ratioframe, resolusi));
                                Imgcodecs.imencode(".jpg", show, mem);
                                java.awt.Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
                                BufferedImage buff = (BufferedImage) im;
                                Image image = SwingFXUtils.toFXImage(buff, null);
                                // end prepare nampilin video asli
                                if(runnable){
                                    videoPanel.setImage(image);
//                                    System.out.println(timeVideo.get(Calendar.MILLISECOND));
                                    setWindow(videoPanel);
//                                    progressBar.setProgress(((double) (timeVideo.get(Calendar.HOUR)*3600 
//                                            + timeVideo.get(Calendar.MINUTE)*60 + timeVideo.get(Calendar.SECOND)) / (double) time));
//                                    if (postFrame % fps == 0) {
//                                        Platform.runLater(() -> {
//                                            timeVideo.set(Calendar.SECOND, second);
//                                            txtMinTime.setText(new SimpleDateFormat("HH:mm:ss").format(timeVideo.getTime()));
//                                            if (second == 60){
//                                                second = 0;
//                                            }
//                                            second++;
//                                        });
//                                    }
                                    progressBar.setProgress(((double) (postFrame1/fps)) / (double) time);
//                                    if (postFrame % fps == 0) {
                                        Platform.runLater(() -> {
                                            int hours = (postFrame/fps) / 3600;
                                            int mins = ((postFrame/fps) % 3600) / 60;
                                            int secs = (postFrame/fps) % 60;
                                            String timeString = String.format("%02d:%02d:%02d", hours, mins, secs);
                                            txtMinTime.setText(timeString);
                                        });
//                                    }
                                } else if (runnable == false) {
                                    this.wait();
                                }
                            } catch(IOException ex){
                                System.out.println("Gaiso nampilno gambar");
                            } catch (InterruptedException ex) {
                                System.out.println("Gaiso run bro");
                            }
                        }
                    }
                    else{
                        Platform.runLater(() -> {
                            Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Information Dialog");
                            alert.setHeaderText(null);
                            alert.setContentText("Proses deteksi gerakan sudah selesai.");
                            alert.showAndWait();
                            initialUlang();
                        });
                        t.stop();
                    }
                    postFrame++;
                }
            }
        }
    }
  
    private void initialUlang(){
        btnStop.setVisible(false);
        videoPanel.setVisible(false);
        txtNowPlaying.setText("Now Playing : ");
        videoProsesPanel.setVisible(false);
        btnAddMedia.setVisible(true);
        btnAddMedia2.setVisible(true);
        btnNewROI.setDisable(false);
        imgLoading.setVisible(false);
        btnProses.setDisable(false);
        btnProses.setText("Proccess");
        txtMinTime.setText("00:00:00");
        txtMaxTime.setText("00:00:00");
        progressBar.setProgress(0);
        video = null;
        mem = null;
        myThread = null;
        postFrame = 0;
        postFrame1 = 0;
        postFrame2 = 0;
        second = 0;
        cnts = null;
        id = 1;
        clip = new ClipVideo();
        roiAda = false;
        t = null;
        settingPane.setVisible(false);
        Image btnSet = new Image("/image/setting_exited.png");
        btnSetting.setImage(btnSet);
        btnSetting.setDisable(false);
    }
    @FXML
    private void reset(){
//        myThread.runnable = false;
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Anda yakin me-reset video dan data hasil?");
        Optional<ButtonType> act = alert.showAndWait();
        if(act.get() == ButtonType.OK){
            tabelHasil.getItems().clear();
            initialUlang();
            File f = new File("E:\\KULIAH\\Tugas Akhir\\Data Video\\dummy\\");
            File[] fs = f.listFiles();
            for(File i:fs){
                i.delete();
            }
            try{
                if(t != null){
                    t.stop();
                }
            } catch(Exception e){
                System.out.println("exception restart");
            }
        } else{
           
        }
    }
    
    private Mat operasiOR(Mat a, Mat b){
        Mat result = new Mat();
        result = Mat.zeros(a.size(), CV_8UC1);
        for (int j = 0; j < a.rows(); j++) {
            for (int i = 0; i < a.cols(); i++) {
                int x = (int) a.get(j, i)[0] | (int) b.get(j, i)[0];
                result.put(j, i, x);
            }
        }
        return result;
    }
    private Mat operasiAND(Mat a, Mat b){
        Mat result = new Mat();
        result = Mat.zeros(a.size(), CV_8UC1);
        for (int j = 0; j < a.rows(); j++) {
            for (int i = 0; i < a.cols(); i++) {
                int x = (int) a.get(j, i)[0] | (int) b.get(j, i)[0];
                result.put(j, i, x);
            }
        }
        return result;
    }
    
    private int getOtsuThresh(Mat gray){
        Mat coba = new Mat();
        gray.copyTo(coba);
        // prepare untuk proses menentukan nilai T dengan metode otsu
        MatOfByte temp = new MatOfByte();
        Imgcodecs.imencode(".bmp", coba, temp);
	
        BufferedImage grayImg = null;
        try {
            grayImg = ImageIO.read(new ByteArrayInputStream(temp.toArray()));
        } catch (IOException ex) {
            System.err.println(ex);
        }
//        Raster raster = grayImg.getData();
//        DataBuffer buffer = raster.getDataBuffer();
//        DataBufferByte byteBuffer = (DataBufferByte) buffer;
//        byte[] srcData = byteBuffer.getData(0);
        // atau langsung ini untuk dapatkan raw image data dari grayImg
        byte[] srcData = ((DataBufferByte) grayImg.getRaster().getDataBuffer()).getData(0);
        //proses inti
        int[] histData = new int[256];
        //mencari level dengan nilai tertinggi
        for(int i = 0; i<srcData.length; i++){
            int levelGray = 0xFF & srcData[i];
            histData[levelGray] ++;
        }
        
        float sum = 0;
        for(int i = 0; i< 256; i++){
            sum += i * histData[i];
        }
        
        float sumB = 0;
        int wB = 0;
        int wF = 0;
        float varMax = 0;
        
        //mencari nilai Thresh
        int thresh = 0;
        for(int i = 0; i< 256; i++){
            wB += histData[i]; //weight background
            if(wB == 0){
                continue;
            }
            wF = srcData.length - wB;   //weight objek (foreground)
            if(wF == 0){
                break;
            }
            sumB += (float) (i * histData[i]);
            float mB = sumB / wB; //mean background
            float mF = (sum - sumB) / wF; //mean objek (foreground)
            
            //menghitung varians antar kelas
            float varians = (float)wB * (float)wF * (mB - mF) * (mB - mF);
            
            //mengudpate variansiTertinggi
            if(varians > varMax){
                varMax = varians;
                thresh = i;
            }
        }
        return thresh;
    }
    
    private void threeFrameDiff(Mat frame1, Mat frame2, Mat frame3){
//        System.out.println("===========\npostframe Bismillah : "+postFrame);
        Core.absdiff(frame2, frame1, deltaFrame1); // frame difference frame ke n dan n-1 disimpan ke deltaFrame1
//        int thresh = getOtsuThresh(deltaFrame1);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\otsu\\diff1_" + postFrame2 + ".png", deltaFrame1);
        Imgproc.threshold(deltaFrame1, deltaFrame1, ambangBatas, 255, Imgproc.THRESH_BINARY);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\otsu\\threshold_diff1_" + postFrame2 + ".png", deltaFrame1);
        
        Core.absdiff(frame3, frame2, deltaFrame2); // frame difference frame ke n+1 dan n disimpan ke deltaFrame2
//        int thresh2 = getOtsuThresh(deltaFrame2);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\otsu\\diff2_" + postFrame2 + ".png", deltaFrame2);
        Imgproc.threshold(deltaFrame2, deltaFrame2, ambangBatas, 255, Imgproc.THRESH_BINARY);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\otsu\\threshold_diff2_" + postFrame2 + ".png", deltaFrame2);
        
//        Mat matY = operasiAND(deltaFrame1,deltaFrame2);
        Mat matY = new Mat();
        Core.bitwise_and(deltaFrame1, deltaFrame2, matY);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\1MatY_" + postFrame2 + ".png", matY);
//        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5,5));
//        Imgproc.morphologyEx(matY, matY, Imgproc.MORPH_CLOSE, kernel);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\2ClosingMatY_" + postFrame2 + ".png", matY);
        
        Mat matM1 = new Mat();
//modifikasi algoritma
        Core.bitwise_xor(matY, deltaFrame2, matM1);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\2MatM1_" + postFrame2 + ".png", matM1);
//        Mat matM1 = operasiOR(matY,deltaFrame2);
        Mat matM2 = new Mat();
        Core.bitwise_or(deltaFrame1, deltaFrame2, matM2);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\3MatM2_" + postFrame2 + ".png", matM2);
//        Mat matM2 = operasiOR(deltaFrame1,deltaFrame2);
        Mat matZ = new Mat();
        Core.bitwise_or(matM1, matM2, matZ);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\4Mat_hasil_" + postFrame2 + ".png", matZ);
//        Mat matZ = operasiOR(matM1, matM2);
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(10,10));
        Imgproc.morphologyEx(matZ, matZ, Imgproc.MORPH_CLOSE, kernel);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\5CloseMatZ_" + postFrame2 + ".png", matZ);
        Imgproc.morphologyEx(matZ, matZ, Imgproc.MORPH_OPEN, kernel);
//        Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\6HasilProsesMorfologi_" + postFrame2 + ".png", matZ);
        hasilProses = matZ;
//        hasilProses = matY;
        
        cnts = new ArrayList<>();
        Imgproc.findContours(hasilProses, cnts, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        boolean motion = false;
        for(int i=0; i < cnts.size(); i++) {
            double area = contourArea(cnts.get(i), false);
            if(area >= minArea) {
                motion = true;
                Rect r = Imgproc.boundingRect(cnts.get(i));
                Imgproc.rectangle(hasilProses, r, new Scalar(255, 255, 255), 2,0);
//                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\Vid4_7DeteksiProses_" + postFrame2 + ".png", hasilProses);
                
//                Imgproc.rectangle(show, new Point(r.x + roi.x, r.y + roi.y), new Point(r.x + roi.x + r.width, r.y + roi.y + r.height), new Scalar(255, 255, 255), 2);
//                double[] loc = {r.x + roi.x, r.y + roi.y, r.width, r.height};
//                r.set(loc);
//                Imgproc.putText(show, "Areas: " + area , new Point(r.x, r.y-3), Imgproc.FONT_HERSHEY_TRIPLEX, 0.3, new Scalar(255, 255, 255));
//                Imgcodecs.imwrite("E:\\KULIAH\\Tugas Akhir\\Draft TA\\Asset Untuk Buku\\three\\Vid4_8DeteksiObjek_" + postFrame2 + ".png", show);
                break;
            }
        }
        //show proses
        if(videoProsesPanel.isVisible()){
            try {
                Imgcodecs.imencode(".jpg", hasilProses, mem);
                java.awt.Image im = ImageIO.read(new ByteArrayInputStream(mem.toArray()));
                BufferedImage buff = (BufferedImage) im;
                Image image = SwingFXUtils.toFXImage(buff, null);
                // end prepare nampilin video asli
                videoProsesPanel.setImage(image);
                setWindow(videoProsesPanel);
            } catch (IOException ex) {
                System.out.println("exception IO, metode");
            }
        }
        
        // kalau udah frame terakhir gerakan diset false;
        if((postFrame/fps) == time){
            motion = false;
        }
        
        // menyimpan interval waktu dan create objek clip video
        if(tabelHasil.getItems().isEmpty()){
            if(motion){
//                System.out.println("BERGERAK CUK");
                if(clip._isnull()){
                    clip = new ClipVideo(id, (postFrame1/fps), (postFrame1/fps));
                }
            } else{
//                System.out.println("ga obah blas sepi");
                if(!clip._isnull()){
                    clip.setDetikAkhir(postFrame/fps);
                    tabelHasil.getItems().add(clip);
                    String dummyPath = "E:\\KULIAH\\Tugas Akhir\\Data Video\\dummy\\clipID_"+clip.getId()+typeFile;
                    try {
                        clip.trimVideo(path, dummyPath);
                    } catch (IOException ex) {
                        System.out.println("exception trim");
                    }
                    System.out.println(clip.getInfo());
                    clip = new ClipVideo();
                }
            }
        } else{
            int i = tabelHasil.getItems().size();
            if(motion){
//                System.out.println("BERGERAK CUK");
                if(clip._isnull()){
                    clip = new ClipVideo(i+1, (postFrame1/fps), (postFrame1/fps));
                }
            } else{
//                System.out.println("ga obah blas sepi");
                if(!clip._isnull()){
                    clip.setDetikAkhir(postFrame/fps);
                    tabelHasil.getItems().add(clip);
                    String dummyPath = "E:\\KULIAH\\Tugas Akhir\\Data Video\\dummy\\clipID_"+clip.getId()+typeFile;
                    try {
                        clip.trimVideo(path, dummyPath);
                    } catch (IOException ex) {
                        System.out.println("exception trim");
                    }
                    System.out.println(clip.getInfo());
                    clip = new ClipVideo();
                }
            }
        }
    }
}
