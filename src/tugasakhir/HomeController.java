package tugasakhir;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
//import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author A S U S
 */
public class HomeController implements Initializable {
//    @FXML
//    private Button btnClose;
//    private Button btnMin;
//    private Button btnStart;
    
    private final int widthStage = 1020;
    private final int heightStage = 617;
    
    @FXML
    private void btnStartOnClick(ActionEvent event) throws IOException{
        Parent home = FXMLLoader.load(getClass().getResource("App.fxml"));
        Scene homeScene = new Scene(home);
        Stage appStage = (Stage)((Node) event.getSource()).getScene().getWindow();
        appStage.hide();
        appStage.setResizable(true);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        appStage.setMaxHeight((int) screen.getHeight());
        appStage.setMaxWidth((int) screen.getWidth());
        appStage.setScene(homeScene);
        
        int x = (int) ((screen.getWidth() - widthStage)/2);
        int y = (int) ((screen.getHeight() - heightStage)/2);
        appStage.setX(x);
        appStage.setY(y);
        appStage.show();
    }
    
    @FXML
    private void btnCloseOnClick(){
        System.exit(0);
    }
    
    @FXML
    private void btnMinOnClick(ActionEvent evt){
        Stage s = (Stage)((Node)evt.getSource()).getScene().getWindow();
        s.setIconified(true);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        File f = new File("E:\\KULIAH\\Tugas Akhir\\Data Video\\dummy\\");
        File[] fs = f.listFiles();
        for(File i:fs){
            i.delete();
        }
    }   
}
