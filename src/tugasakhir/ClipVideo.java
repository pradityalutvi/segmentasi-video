package tugasakhir;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author A S U S
 */
public class ClipVideo {
    private int id;
    private String detikAwal, detikAkhir;
    
    public ClipVideo(){
        this.id = 0;
        this.detikAwal = null;
        this.detikAkhir = null;
    }
    
    public ClipVideo(int id, int detikAwal, int detikAkhir){
        this.id = id;
        this.detikAwal = convertSecsToTimeString(detikAwal);
        this.detikAkhir = convertSecsToTimeString(detikAkhir);
    }

    public boolean _isnull(){
        if(id == 0){
            return true;
        } else{
            return false;
        }
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetikAwal() {
        return detikAwal;
    }

    public void setDetikAwal(int detikAwal) {
        this.detikAwal = convertSecsToTimeString(detikAwal);
    }
    
    public String getDetikAkhir() {
        return detikAkhir;
    }

    public void setDetikAkhir(int detikAkhir) {
        this.detikAkhir = convertSecsToTimeString(detikAkhir);
    }
    
    public void trimVideo(String vidPathSrc, String vidPathDesc) throws IOException{
        //ini berhasil// ffmpeg -i input.mp4 -ss waktuawal -to waktuakhir -c copy output.mp4
        // ffmpeg -ss framepos/fps -i input.mp4 -c:v libx264 -c:a aac -frames:v totalframedipotong out.mp4
        // ffmpeg -i input.mp4 -vf select="between(n\,frameawal\,frameakhir\),setpts=PTS-STARTPTS" out.mp4
//        System.out.println("cok");
        String ffmpegEXE = "C:\\ffmpeg\\bin\\ffmpeg.exe";
        List<String> command = new ArrayList<>();
        command.add(ffmpegEXE);
        command.add("-i");
        command.add(vidPathSrc);
//        command.add("-vf");
//        command.add("select=\"between(n\\,"+this.frameAwal+"\\,"+this.frameAkhir+")setpts=PTS-STARTPTS\"");
        
        command.add("-ss");
        command.add(this.detikAwal);
        command.add("-to");
        command.add(this.detikAkhir);
        command.add("-c");
//        command.add("-c:v");
//        command.add("copy");
//        command.add("-c:a");
        command.add("copy");
        command.add(vidPathDesc);
        
        ProcessBuilder builder = new ProcessBuilder(command);
        Process proses = builder.start();
//            //Call ffmpeg to create this chunk of the video using a ffmpeg wrapper
//            String argv[] = {"ffmpeg", "-i", videoPath, 
//                    "-ss",startTime, "-t", endTime,
//                    "-c","copy", segmentVideoPath[i]};
//            int ffmpegWrapperReturnCode = ffmpegWrapper(argv);
    }
    
    public String getInfo(){
        String st = "id = "+this.id+", awal = "+this.detikAwal+" - akhir = "+this.detikAkhir;
        return st;
    }
    
    public String convertSecsToTimeString(int timeSeconds) {
        //Convert number of seconds into hours:mins:seconds string
        int hours = timeSeconds / 3600;
        int mins = (timeSeconds % 3600) / 60;
        int secs = timeSeconds % 60;
        String timeString = String.format("%02d:%02d:%02d", hours, mins, secs);
        return timeString;
    }    
}
